package com.demo.utils.enums

object GreetingTypes {
  val WELCOME = "welcome"
  val FORMAL = "formal"
  val SEMIFORMAL = "semiformal"
  val INFORMAL = "informal"
}
