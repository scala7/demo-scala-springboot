package com.demo.domain

import scala.beans.BeanProperty

case class Greeting(@BeanProperty var body: String)