package com.demo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Main {}

object Main extends App { SpringApplication.run(classOf[Main], args:_*) }