package com.demo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{CrossOrigin, PathVariable, RequestMapping, RequestMethod, RequestParam, RestController}
import com.demo.domain.Greeting
import com.demo.service.GreetingService
import com.demo.utils.enums.GreetingTypes

@CrossOrigin(origins = Array("*"), maxAge = 2000, allowedHeaders = Array("header1,header2"), exposedHeaders = Array("header1"), allowCredentials = "false")
@RestController
@RequestMapping(value = Array("/greetings"))
class Controller @Autowired()(greetingService: GreetingService) {

  @RequestMapping(value = Array("/"), method = Array(RequestMethod.GET))
  def greeting(): Greeting = greetingService.get(GreetingTypes.WELCOME)

  @RequestMapping(value = Array("/{gType}"), method = Array(RequestMethod.GET))
  def greeting(@PathVariable(value = "gType") gType: String): Greeting = greetingService.get(gType)

}