package com.demo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import com.demo.utils.PropertiesBundle
import com.demo.domain.Greeting
import com.demo.utils.enums.GreetingTypes

@Service
class GreetingService @Autowired()(properties: PropertiesBundle) {

  def get(gType: String): Greeting = gType.toLowerCase match {
      case GreetingTypes.WELCOME => retrieveGreeting(properties.greeting1)
      case GreetingTypes.FORMAL => retrieveGreeting(properties.greeting2)
      case GreetingTypes.SEMIFORMAL => retrieveGreeting(properties.greeting3)
      case GreetingTypes.INFORMAL => retrieveGreeting(properties.greeting4)
      case _ => retrieveGreeting(properties.greeting1)
    }

  private def retrieveGreeting(gType: String) = {
    Greeting(s"Hey!, ${gType}")
  }
}
