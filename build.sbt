name := "scala-springboot-rest"

version := "0.1"

scalaVersion := "2.13.3"

libraryDependencies ++= Seq(
  "org.springframework.boot" % "spring-boot-starter-web" % "2.3.5.RELEASE",
  "org.springdoc" % "springdoc-openapi-ui" % "1.4.8"
)
